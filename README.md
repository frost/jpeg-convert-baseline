This is a tool to batch convert JPEG images from progressive to baseline. This is useful for apps like the Wii's Photo Channel that don't support progressive JPEGs.

### Dependencies
- zsh
- ImageMagick

### Usage
```
jpeg-convert-baseline [--no-replace | --only-replace] [-t] *.jpg
```

#### Options
- `-t`: preserve timestamps (you probably want this on)
- `--no-replace`: don't replace progressive JPEGs with the baseline ones, just leave the baseline ones as `*.baseline`
- `--only-replace`: don't actually do the conversion, only move the .baseline files into place [doesn't need any file arguments]
